#!/bin/python3

"""
LightStore uses LightShot (prnt.sc) to upload and download arbitrary data
by appending payload to PNG image files.
"""

import sys
import logging
import argparse
import lightstore.io

ARGS = None

def size(size_str: str) -> int:
    """
    Convert a size String to the number of bytes.
    Example: '100ki' -> 102400
    """
    if size_str.endswith('Mi'):
        return int(size_str[:-2]) * 1024**2
    if size_str.endswith('M'):
        return int(size_str[:-1]) * 1000**2
    if size_str.endswith('ki'):
        return int(size_str[:-2]) * 1024
    if size_str.endswith('k'):
        return int(size_str[:-1]) * 1000
    return int(size_str)

def get():
    """runs the downloader module using the parsed arguments"""
    lightstore.io.download(ARGS.pointer, ARGS.destination)


def put():
    """runs the uploader module using the parsed arguments"""
    lightstore.io.upload(ARGS.source, ARGS.chunk_size)

def main():
    """
    Create args, parse args, run
    """
    global ARGS
    logging.basicConfig(format='%(message)s')
    logging.getLogger().setLevel(logging.INFO)

    parser = argparse.ArgumentParser(
        description='Uses lightshot (prnt.sc) to upload and download data.')
    parser.set_defaults(func=parser.print_help)
    subparsers = parser.add_subparsers(title='commands')

    parser_get = subparsers.add_parser('get', help='Downloads a file')
    parser_get.set_defaults(func=get)
    parser_get.add_argument('pointer', type=str, help='Pointer of file to download')
    parser_get.add_argument('destination', type=str, help='Path to save to')

    parser_upload = subparsers.add_parser('put', help='Uploads a file')
    parser_upload.set_defaults(func=put)
    parser_upload.add_argument('source', type=str, help='Path to the file to upload')
    parser_upload.add_argument(
        '--chunk-size', type=size,
        help="""Maximum upload chunk size in bytes (default: 5M, tested maximum: 5242701)
        Available units: 1M = 1000*1000, 1Mi = 1024*1024, 1k = 1000, 1ki = 1024""",
        default=5000000)

    # parse args
    ARGS = parser.parse_args()

    # run sub-commands
    ARGS.func()
