"""
Launches main.main() when executed via command line.
"""
from lightstore import main

if __name__ == "__main__":
    main.main()
