"""
Handles uploading and downloading files and streams to and from LightShot.
"""
import os
import sys
import re
import logging
from Crypto.Hash import SHA256
import pkg_resources
import _io

from lightstore import crypto
from lightstore import lightshot_api

def bytes_to_size(num_bytes: int) -> str:
    """
    Converts a numeric file size to string.
    Example: 1000 -> '1.00kB', 1024 -> '1.02kB'
    """
    orders = ['', 'k', 'M', 'G', 'T', 'P']
    result = str(num_bytes)
    for (i, unit) in enumerate(orders):
        factor = 1000**i
        if num_bytes/factor < 1000:
            result = f'{num_bytes/factor:.02f}{unit}B'
            break
    return result

def open_input(path: str) -> _io.BufferedReader:
    """
    Returns the input stream of a path.
    """
    return sys.stdin.buffer if path == '-' else open(path, 'rb')

def open_output(path: str) -> _io.BufferedWriter:
    """
    Returns the output stream of a path.
    """
    return sys.stdout.buffer if path == '-' else open(path, 'wb')

def upload(path: str, chunk_size: int) -> str:
    """
    Reads a file from a given path chunk-by-chunk, encrypts it, appends it to
    a dummy PNG image file and uploads it to LightShot.
    """
    dummy_image = pkg_resources.resource_stream(__name__, 'resources/dummy.png').read()
    logging.info(f'Loaded dummy image ({len(dummy_image)} bytes)')
    b64_key = crypto.generate_b64_key()
    chunk_list = []
    with open_input(path) as source_file:
        file_size = None if path == '-' else os.path.getsize(path)
        logging.info('Uploading chunks...')
        chunk_counter = 0
        byte_counter = 0
        while chunk := source_file.read(chunk_size):
            encrypted_chunk, b64_auth_tag = crypto.encrypt(chunk, b64_key)
            image_id = lightshot_api.upload(dummy_image + encrypted_chunk)
            chunk_list.append(f'{image_id} {b64_auth_tag}')
            chunk_counter += 1
            byte_counter += len(chunk)
            logging.info(f'[{chunk_counter:03}] {bytes_to_size(byte_counter)}/{bytes_to_size(file_size) if file_size else "?"} uploaded')
    index_string = '\n'.join(chunk_list)
    logging.debug('--- Start of index file')
    logging.debug(index_string)
    logging.debug('--- End of index file')
    index_file = index_string.encode()
    encrypted_index_file, b64_auth_tag = crypto.encrypt(index_file, b64_key)
    index_image_id = lightshot_api.upload(dummy_image + encrypted_index_file)
    logging.info('Index uploaded')

    logging.info(f'The pointer to this file is: {index_image_id}-{b64_key}-{b64_auth_tag}')



def get_payload_from_image(img_data):
    """
    Returns the part of a file after the PNG footer (payload).
    """
    png_footer = b'\x49\x45\x4e\x44\xae\x42\x60\x82'
    _, _, payload = img_data.partition(png_footer)
    return payload

POINTER_RE_PATTERN = '^([a-zA-z0-9]+)-([a-zA-Z0-9/\+=]+)-([a-zA-Z0-9/\+=]+)$'
def download(pointer: str, path: str):
    """
    Downloads the data of a pointer into a file or STDOUT.
    """
    if match := re.match(POINTER_RE_PATTERN, pointer):
        index_image_id, password, index_auth_tag = match.groups()
        index_image = lightshot_api.download(index_image_id)
        encrypted_index_file = get_payload_from_image(index_image)
        index_file = crypto.decrypt(encrypted_index_file, password, index_auth_tag)
        index_string = index_file.decode()
        chunk_list = [line.split() for line in index_string.splitlines()]

        with open_output(path) as output_file:
            for i, (chunk_img_id, auth_tag) in enumerate(chunk_list):
                chunk_image = lightshot_api.download(chunk_img_id)
                encrypted_chunk = get_payload_from_image(chunk_image)
                chunk = crypto.decrypt(encrypted_chunk, password, auth_tag)
                output_file.write(chunk)
                logging.info(f'{i+1}/{len(chunk_list)} downlaoded.')
    else:
        logging.info('Pointer is invalid.')
