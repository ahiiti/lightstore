"""
Handles uploading and downloading single image files to and from LightShot.
"""
import os
import requests

HEADERS = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0'}

UPLOAD_URL = 'https://prntscr.com/upload.php'

def upload(image: bytes) -> str:
    """
    Uploads an image to LightShot and returns the image ID.
    """
    response = requests.post(
        UPLOAD_URL,
        files={'image': ('screenshot.png', image, b'image/png')},
        headers=HEADERS)
    response_data = response.json()
    image_url = response_data['data']
    image_id = os.path.split(image_url)[-1]
    return image_id

def get_image_url_by_id(img_id):
    """Returns the url to the image file of a image id"""
    with requests.get(f'https://prnt.sc/{img_id}', headers=HEADERS) as req:
        start_idx = req.text.find('https://image.prntscr.com/image/')
        if start_idx == -1:
            return ''
        end_idx = req.text.find('"', start_idx)
        image_url = req.text[start_idx:end_idx]
        return image_url


def download(img_id):
    """Downloads a single image by it's ID"""
    url = get_image_url_by_id(img_id)
    if url == '':
        return ''
    response = requests.get(url)
    return response.content
