"""
Handles encryption and decryption of data
"""

import string
import random
import logging
import base64
from Crypto.Cipher import AES
from Crypto import Random

def bin_to_b64(bin: bytes):
    """
    Converts bytes to base64 string
    """
    return base64.b64encode(bin).decode()

def b64_to_bin(b64str: str):
    """
    Converts a base64 string to bytes
    """
    return base64.b64decode(b64str.encode())

def encrypt(plaintext: bytes, b64_key: str) -> bytes:
    """
    Encrypt data using a password
    """
    cipher = AES.new(key=b64_to_bin(b64_key), mode=AES.MODE_GCM)
    ciphertext, auth_tag = cipher.encrypt_and_digest(plaintext)
    return cipher.nonce + ciphertext, bin_to_b64(auth_tag)

def decrypt(encrypted: bytes, b64_key: str, b64_auth_tag: str) -> bytes:
    """
    Decrypt and verify data using a password and MAC
    """
    nonce, ciphertext = encrypted[:16], encrypted[16:]
    cipher = AES.new(key=b64_to_bin(b64_key), mode=AES.MODE_GCM, nonce=nonce)
    return cipher.decrypt_and_verify(ciphertext, b64_to_bin(b64_auth_tag))

def generate_b64_key() -> str:
    """
    Randomly generate a password
    """
    return base64.b64encode(Random.get_random_bytes(32)).decode()
