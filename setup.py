#!/usr/bin/env python3

from setuptools import setup, Command
from distutils.spawn import spawn
import sys

setup(name = 'lightstore',
    version = f'0.1',
    description = "Use lightshot (prnt.sc) to store files.",
    long_description = 'Use lightshot (prnt.sc) to upload and download files.',
    platforms = ["Linux"],
    author="ahiiti",
    url="https://gitlab.com/ahiiti/lightstore/",
    license="GPL3",
    packages = ['lightstore'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'lightstore = lightstore.main:main'
        ]
    }
)
