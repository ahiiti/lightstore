lightstore
==========

Use [LightShot](https://prnt.sc) to upload and download arbitrary files.

- - - - - -

Usage
-----

To upload a file, use:

    lightstore put <file_name>

The program will return a string called _pointer_, which you will need to download the file again. Go to 'How it works'/'Pointer' for more information.


To download a file, use:

    lightstore get <pointer> <file_name>

You can also use '-' as file name to read from STDIN and write to STDOUT.

- - - - - -

Examples
--------

Upload and download `archive.zip`:

    $ lightstore put archive.zip
    The pointer is: upe6j3-W9URvZuLANYxnTl37nVv9rFm...

    $ lightstore get upe6j3-W9URvZuLANYxnTl37nVv9rFm... archive1.zip

Stream media from LightShot using pipes:

    $ youtube-dl -f best https://www.youtube.com/watch?v=w0jyU13gtBs -o - | lightstore put -
    Pointer: upe6j3-W9URvZuLANYxnTl37nVv9rFm...

    $ lightstore get upe6j3-W9URvZuLANYxnTl37nVv9rFm... - | vlc -

- - - - - -

How it works
------------

The program appends payload data to PNG files.
LightShot stores PNG files without re-compressing them.
Even data following the actual PNG image doesn't get removed.

By downloading the image and stripping away the PNG file, we get back our data.

Apparently, LightShot accepts PNG files of up to ~5.2MB.
The program splits data into chunks of 5MB by default.

### Encryption

The program uses 256-bit
[AES-GCM (Galois/Counter Mode)](https://en.wikipedia.org/wiki/Galois/Counter_Mode)
to encrypt each payload of data.

AES-GCM generates a MAC (Message Authentication Code) for each encrypted payload.
Using the MAC, the decryption process automatically verifies data integrity.

### The index file

Because the data is split into chunks, we need an index file listing these chunks.

For each chunk, the index file stores:
 - The image ID
 - The MAC for the encrypted payload

Uploading a 12MB file may look like this:

| Chunk | Payload size | Upload size | Image Address            | Image ID | base64-encoded MAC       |
|-------|--------------|-------------|--------------------------|----------|--------------------------|
| 1     | 5000000      | 5000321     | `https://prnt.sc/123008` | 123008   | OK0SbvficRNxn5o1xoqN8Q== |
| 2     | 5000000      | 5000321     | `https://prnt.sc/123009` | 123009   | xOIkHv9HzH9P2jM0Qq/SCw== |
| 3     | 2000000      | 2000321     | `https://prnt.sc/12300a` | 12300a   | BbMumDPBB9EtL5FfXlme9g== |

Resulting in the following index file:

    123008 OK0SbvficRNxn5o1xoqN8Q==
    123009 xOIkHv9HzH9P2jM0Qq/SCw==
    12300a BbMumDPBB9EtL5FfXlme9g==

### The pointer

After uploading a file, lightstore returns a token called _pointer_.
You will need the pointer to be able to download the file again.

The pointer file contains following information:

    Image ID of                                         Base64-encoded MAC
    the index file                                      of the index file
    ------                                              ------------------------
    upe6j3-W9URvZuLANYxnTl37nVv9rFm4Sz+n8hW9V1Yh6rVG5A=-BbMumDPBB9EtL5FfXlme9g==
           --------------------------------------------
           Base64-encoded decryption key
